'use strict';
const api = new (require('./api'));

console.log("BestMonthToBuy : (apple) " + (api.bestMonthToBuyStock('aapl') == 'February'));
console.log("BestMonthToBuy : (microsoft) " + (api.bestMonthToBuyStock('msft') == 'October'));

var symbolMap = require('./symbol_map.json');
var data = require('./' + symbolMap['apple'] + '.json');