'use strict'

var https = require('https');
var q = require('q');
var request = require('request-promise');
var errors = require('request-promise/errors');
var moment = require('moment');
var companyNames = require('./symbol_map.json');
var CONSTANTS = require('./constants');

var month_array = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
var start_date_array = '01';
var end_date_array = ['31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31'];
var year_array = ['2012', '2013', '2014', '2015', '2016'];
var month_convertor = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

function ApiManager() {
}


var runSMA = function (arr) {
    const SMA = require('technicalindicators').SMA;
    let period = 8;

    var mCheck = SMA.calculate({period: period, values: arr});

    var interpolated_arr = mCheck[mCheck.length - 1];
    var original_arr = arr[arr.length - 1];

    console.log("interpolated array " + mCheck[mCheck.length - 1]);
    console.log("original array " + arr[arr.length - 1]);

    if (original_arr > interpolated_arr) {
        console.log(CONSTANTS.BUY);
        return CONSTANTS.BUY;
    } else {
        console.log(CONSTANTS.SELL);
        return CONSTANTS.SELL;
    }
};

var runEMA = function (arr) {
    const EMA = require('technicalindicators').EMA;

    let period = 8;

    var mCheck = EMA.calculate({period: period, values: arr});

    var interpolated_arr = mCheck[mCheck.length - 1];

    var original_arr = arr[arr.length - 1];

    console.log("interpolated array EMA " + mCheck[mCheck.length - 1]);
    console.log("original array EMA " + arr[arr.length - 1]);

    if (original_arr > interpolated_arr) {
        console.log(CONSTANTS.BUY);
        return CONSTANTS.BUY;
    } else {
        console.log(CONSTANTS.SELL);
        return CONSTANTS.SELL;
    }
};

var runWMA = function (arr) {
    const WMA = require('technicalindicators').WMA

    let period = 8;

    var mCheck = WMA.calculate({period: period, values: arr});

    var interpolated_arr = mCheck[mCheck.length - 1];

    var original_arr = arr[arr.length - 1];

    console.log("interpolated array WMA " + mCheck[mCheck.length - 1]);
    console.log("original array WMA " + arr[arr.length - 1]);

    if (original_arr > interpolated_arr) {
        console.log(CONSTANTS.BUY);
        return CONSTANTS.BUY;
    } else {
        console.log(CONSTANTS.SELL);
        return CONSTANTS.SELL;
    }
};

var runEMAForValue = function (arr) {
    const EMA = require('technicalindicators').EMA;

    let period = 8;

    var mCheck = EMA.calculate({period: period, values: arr});

    var interpolated_arr = mCheck[mCheck.length - 1];

    var original_arr = arr[arr.length - 1];

    console.log("interpolated array EMA " + mCheck[mCheck.length - 1]);
    console.log("original array EMA " + arr[arr.length - 1]);

    return (original_arr - interpolated_arr);
};

var runWMAForValue = function (arr) {
    const WMA = require('technicalindicators').WMA

    let period = 8;

    var mCheck = WMA.calculate({period: period, values: arr});

    var interpolated_arr = mCheck[mCheck.length - 1];

    var original_arr = arr[arr.length - 1];

    console.log("interpolated array WMA for Value " + mCheck[mCheck.length - 1]);
    console.log("original array WMA for Value " + arr[arr.length - 1]);

    return (original_arr - interpolated_arr);
};

var runSMAForValue = function (arr) {
    const SMA = require('technicalindicators').SMA;
    let period = 8;

    var mCheck = SMA.calculate({period: period, values: arr});

    var interpolated_arr = mCheck[mCheck.length - 1];
    var original_arr = arr[arr.length - 1];

    console.log("interpolated array " + mCheck[mCheck.length - 1]);
    console.log("original array " + arr[arr.length - 1]);

    return (original_arr - interpolated_arr);
};


ApiManager.prototype.averageInterpolator = function (company, years) {
    var endYear = 2016;
    var startYear = 2016 - years;
    var arr = [];
    var data = require('./' + company + '.json');
    for (var i = 0; i < 12; i++) {
        for (var j = startYear; j <= endYear; j++) {
            var theDate = (startYear + "") + month_array[i] + end_date_array[i];
            if (data.resultMap.RETURNS[0].returnsMap[theDate]) {
                var price = data.resultMap.RETURNS[0].returnsMap[theDate]['level'];
                arr.push(price);
            }
        }
    }

    var array_to_return = {};
    array_to_return.SMA = runSMA(arr);
    array_to_return.EMA = runEMA(arr);
    array_to_return.WMA = runWMA(arr);
    array_to_return.MACD = runMACD(arr);

    return array_to_return;
}


ApiManager.prototype.bestStockToBuy = function (years) {
    var endYear = 2016;
    var startYear = 2016 - years;
    var arr = [];

    var main_result_sma = 0.0;
    var company_name_sma = "None";

    var main_result_ema = 0.0;
    var company_name_ema = "None";

    var main_result_wma = 0.0;
    var company_name_wma = "None";
    var inputRequire = {};
    for (var key in companyNames) {
        var company = companyNames[key];
        inputRequire[company] = inputRequire[company] || require('./' + company + '.json');
        var data = inputRequire[company];
        var returnMap = data.resultMap.RETURNS[0].returnsMap;
        for (var i = 0; i < 12; i++) {
            for (var j = startYear; j <= endYear; j++) {
                var theDate = (startYear + "") + month_array[i] + end_date_array[i];
                if (returnMap[theDate]) {
                    var price = returnMap[theDate]['level'];
                    arr.push(price);
                }
            }
        }

        var the_return_arr = [];

        var array_result = 0.0;
        array_result = runSMAForValue(arr);
        if (array_result > main_result_sma) {
            main_result_sma = array_result;
            company_name_sma = company;
        }

        array_result = runEMAForValue(arr);
        if (array_result > main_result_ema) {
            main_result_ema = array_result;
            company_name_ema = company;
        }

        array_result = runWMAForValue(arr);
        if (array_result > main_result_wma) {
            main_result_wma = array_result;
            company_name_wma = company;
        }

        the_return_arr.SMA = company_name_sma;
        the_return_arr.EMA = company_name_ema;
        the_return_arr.WMA = company_name_wma;
        arr = [];
    }
    return the_return_arr;
};


ApiManager.prototype.bestStockToSell = function (years) {
    var endYear = 2016;
    var startYear = 2016 - years;
    var arr = [];
    var main_result_sma = 0.0;
    var company_name_sma = "None";

    var main_result_ema = 0.0;
    var company_name_ema = "None";

    var main_result_wma = 0.0;
    var company_name_wma = "None";

    var inputRequire = {};
    for (var key in companyNames) {
        var company = companyNames[key];
        inputRequire[company] = inputRequire[company] || require('./' + company + '.json');
        var data = inputRequire[company];
        var returnMap = data.resultMap.RETURNS[0].returnsMap;
        for (var i = 0; i < 12; i++) {
            for (var j = startYear; j <= endYear; j++) {
                var theDate = (startYear + "") + month_array[i] + end_date_array[i];
                if (returnMap[theDate]) {
                    var price = returnMap[theDate]['level'];
                    arr.push(price);
                }
            }
        }

        var the_return_arr = [];

        var array_result = 0.0;
        array_result = runSMAForValue(arr);
        if (array_result < main_result_sma) {
            main_result_sma = array_result;
            company_name_sma = company;
        }

        array_result = runEMAForValue(arr);
        if (array_result < main_result_ema) {
            main_result_ema = array_result;
            company_name_ema = company;
        }

        array_result = runWMAForValue(arr);
        if (array_result < main_result_wma) {
            main_result_wma = array_result;
            company_name_wma = company;
        }

        the_return_arr.SMA = company_name_sma;
        the_return_arr.EMA = company_name_ema;
        the_return_arr.WMA = company_name_wma;

        arr = [];
    }

    return the_return_arr;
};


ApiManager.prototype.bestMonthToBuyStock = function (company) {
    let bestMonth = 0;
    let bestValue = 0.0;
    for (var i = 0; i < 12; i++) {
        var tempBestValue = 0.0;
        for (let j = 0; j < 5; j++) {
            let startDate = year_array[j] + month_array[i] + start_date_array;
            let endDate = year_array[j] + month_array[i] + end_date_array[i];
            let result = Number(this.getStockChangePercentage(company, startDate, endDate));
            tempBestValue = tempBestValue + result;
        }
        if (tempBestValue > bestValue) {
            bestValue = tempBestValue;
            bestMonth = i;
        }
    }

    console.log("API call : bestMonthToBuyStock : " + month_convertor[bestMonth]);
    return month_convertor[bestMonth];
};

ApiManager.prototype.worstMonthToBuyStock = function (company) {
    let worstMonth = 0;
    let worstValue = Infinity;
    for (let i = 0; i < 12; i++) {
        let tempWorstValue = 0;
        for (let j = 0; j < 5; j++) {
            let startDate = year_array[j] + month_array[i] + start_date_array;
            let endDate = year_array[j] + month_array[i] + end_date_array[i];
            let result = Number(this.getStockChangePercentage(company, startDate, endDate));
            tempWorstValue = tempWorstValue + result;
        }
        console.log("worstValue" + month_convertor[i] + tempWorstValue);
        if (tempWorstValue < worstValue) {
            worstValue = tempWorstValue;
            worstMonth = i;
        }
    }

    console.log("API call : worstMonthToBuyStock : " + month_convertor[worstMonth]);
    return month_convertor[worstMonth];
};

//will the stocks fall or raise next month

ApiManager.prototype.comparePerformance = function (company1, company2, years) {
    var startDate = startYear + "1231";
    var endDate = '20161231';

    if (years <= 5) {
        var startYear = (2016 - years) + "";
        startDate = startYear + "1231";
        endDate = '20161231';
    } else {
        var startYear = (years - 1) + "";
        startDate = startYear + "1231";
        endDate = years + "1231";
    }

    var result1 = Number(this.getStockChangePercentage(company1, startDate, endDate));
    var result2 = Number(this.getStockChangePercentage(company2, startDate, endDate));

    console.log(company1 + result1);
    console.log(company2 + result2);
    var result = [result1, result2];
    return result;
};

ApiManager.prototype.highestGrowthYear = function (company) {

    var year = {
        year1: '20111231',
        year2: '20121231',
        year3: '20131231',
        year4: '20141231',
        year5: '20151231',
        year6: '20161231'
    };
    var resultYear = '20121231';
    var result = 0.0;

    for (var i = 1; i < 6; i++) {
        var j = i + 1;
        var tempResult = Number(this.getStockChangePercentage(company, year['year' + i], year['year' + j]));
        if (tempResult >= result) {
            resultYear = year['year' + j].substr(0, 4);
            result = tempResult;
        }
    }
    console.log("API call : highestGrowthYear " + resultYear + "      " + result);
    return [resultYear, result];
};

//year has to be less than 2017
ApiManager.prototype.highestReturningStock = function (year) {
    var currentEndDate = year + "1231";
    var currentStartDate = year - 1 + "1231";

    if (year <= 5) {
        currentEndDate = "20161231";
        currentStartDate = (2016 - year) + "1231";
    } else {
        currentEndDate = year + "1231";
        currentStartDate = (year - 1) + "1231";
    }

    var bestCompany = undefined;
    var result = 0.0;

    for (var key in companyNames) {
        //bestCompany = key;
        var tempResult = Number(this.getStockChangePercentage(companyNames[key], currentStartDate, currentEndDate));
        if (tempResult > result) {
            result = tempResult;
            bestCompany = key;
        }
    }
    console.log(bestCompany + " " + result);
    return [bestCompany, result];
};

ApiManager.prototype.performanceInFiveYears = function (identifier) {
    var currentStartDate = "20111231";
    var currentEndDate = "20161231";
    var result = Number(this.getStockChangePercentage(identifier, currentStartDate, currentEndDate));
    console.log(result);
    return result;
};

ApiManager.prototype.fetchPerformanceByYear = function (identifier, year) {
    var currentEndDate = year + "1231";
    var currentStartDate = year - 1 + "1231";
    var result = Number(this.getStockChangePercentage(identifier, currentStartDate, currentEndDate));
    console.log(result);
    return result;
};

ApiManager.prototype.getStockChangePercentage = function (symbol, startDate, EndDate) {
    let mStartingPrice = null;
    let mEndPrice = null;

    var data = require('./' + symbol + '.json');
    if (data.resultMap.RETURNS[0].returnsMap[startDate]) {
        mStartingPrice = data.resultMap.RETURNS[0].returnsMap[startDate]['level'];
    } else {
        return "Date not found in the database";
    }
    if (data.resultMap.RETURNS[0].returnsMap[EndDate]) {
        mEndPrice = data.resultMap.RETURNS[0].returnsMap[EndDate]['level'];
    } else {
        return "Date not found in the database";
    }
    let mPercentage = (mEndPrice - mStartingPrice) * 100 / mStartingPrice;
    console.log(mPercentage.toFixed(2));
    return mPercentage.toFixed(2);
};

var runMACD = function (arr) {
    var MACD = require('technicalindicators').MACD;
    var macdInput = {
        values: arr,
        fastPeriod: 5,
        slowPeriod: 8,
        signalPeriod: 3,
        SimpleMAOscillator: false,
        SimpleMASignal: false
    };

    var macd = MACD.calculate(macdInput);
    var macdFinalRow = macd[macd.length - 1];
    if (macdFinalRow.MACD && macdFinalRow.signal) {
        if (macdFinalRow.MACD > macdFinalRow.signal) {
            return CONSTANTS.BUY;
        } else {
            return CONSTANTS.SELL;
        }
    } else {
        return undefined;
    }
};

module.exports = ApiManager;