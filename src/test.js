'use strict';

var api = new (require('./api'));

const EMA = require('technicalindicators').EMA;
let period = 8;
let values = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
var result = EMA.calculate({period: period, values: values});
console.log(result);


var MACD = require('technicalindicators').MACD;
var macdInput = {
    values: [127.75, 129.02, 132.75, 145.40, 148.98, 137.52, 147.38, 139.05, 137.23, 149.30, 162.45, 178.95, 200.35, 221.90, 243.23, 243.52, 286.42, 280.27],
    fastPeriod: 5,
    slowPeriod: 8,
    signalPeriod: 3,
    SimpleMAOscillator: false,
    SimpleMASignal: false
};

var result = MACD.calculate(macdInput);
console.log("MACD : ");
console.log(result);

api.getStockChangePercentage('msft', 20161130, 20161231);


//api.fetchPerformance('AAPL');

api.getStockChangePercentage('bkcc', 20161130, 20161231);


api.fetchPerformanceByYear('bkcc', '2015');


api.fetchPerformanceByYear('bkcc', '2015');


api.performanceInFiveYears('bkcc');


api.highestGrowthYear('msft');

api.comparePerformance('aapl', 'bkcc', 2);


api.comparePerformance('bkcc', 'dis', 2013);


api.highestReturningStock(2012);


var y = api.highestReturningStock(3);
console.log("bkcc adarsh" + y);

console.log( "bestMonthToBuyStock" + api.bestMonthToBuyStock('coke'));

console.log("worstMonthToBuyStock" + api.worstMonthToBuyStock('bkcc'));

console.log(api.bestMonthToBuyStock('aapl'));

console.log(api.worstMonthToBuyStock('aapl'));

/*const SMA = require('technicalindicators').SMA
 let period = 8;
 let values = [1,2,3,4,5,6,7,8,9,10,11,12,13];

 var mCheck = SMA.calculate({period : period, values : values});

 console.log(mCheck);*/

console.log("averageInterpolator");
console.log(api.averageInterpolator('bkcc', 3));
console.log(api.averageInterpolator('aapl', 9));
/*console.log(api.averageInterpolator('goog', 3));*/
//api.averageInterpolator('aapl', 3);


console.log(api.bestStockToBuy(4));

console.log(api.bestStockToSell(4));



//Testing dynamic requires
var symbolMap = require('./symbol_map.json');
var data = require('./' + symbolMap['apple'] + '.json');
// console.log(data);