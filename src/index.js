'use strict';
const Alexa = require('alexa-sdk');
const api = new (require('./api'));
const symbolMap = require('./symbol_map.json');
const CONSTANTS = require('./constants');
const TOTAL_TECH_INDICATORS = 4;

//=========================================================================================================================================
//TODO: The items below this comment need your attention
//=========================================================================================================================================

//Replace with your app ID (OPTIONAL).  You can find this value at the top of your skill's page on http://developer.amazon.com.  
//Make sure to enclose your value in quotes, like this:  var APP_ID = "amzn1.ask.skill.bb4045e6-b3e8-4133-b650-72923c5980f1";
var APP_ID = "amzn1.ask.skill.a4c6655e-02d9-40c3-aea9-beb0f266470d";

//This is a list of positive speechcons that this skill will use when a user gets a correct answer.  For a full list of supported
//speechcons, go here: https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/speechcon-reference
var speechConsCorrect = ["Booya", "All righty", "Bam", "Bazinga", "Bingo", "Boom", "Bravo", "Cha Ching", "Cheers", "Dynomite",
    "Hip hip hooray", "Hurrah", "Hurray", "Huzzah", "Oh dear.  Just kidding.  Hurray", "Kaboom", "Kaching", "Oh snap", "Phew",
    "Righto", "Way to go", "Well done", "Whee", "Woo hoo", "Yay", "Wowza", "Yowsa"];

//This is a list of negative speechcons that this skill will use when a user gets an incorrect answer.  For a full list of supported
//speechcons, go here: https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/speechcon-reference
var speechConsWrong = ["Argh", "Aw man", "Blarg", "Blast", "Boo", "Bummer", "Darn", "D'oh", "Dun dun dun", "Eek", "Honk", "Le sigh",
    "Mamma mia", "Oh boy", "Oh dear", "Oof", "Ouch", "Ruh roh", "Shucks", "Uh oh", "Wah wah", "Whoops a daisy", "Yikes"];

//This is the welcome message for when a user starts the skill without a specific intent.
var WELCOME_MESSAGE = "Welcome to Blackrock Aladdin, the best end-to-end investment platform in the world!";

//This is the message a user will hear when they try to cancel or stop the skill, or when they finish.
var EXIT_SKILL_MESSAGE = "Thank you for using Aladdin!  Let's meet again soon!";

//This is the message a user will hear when they ask Alexa for help in your skill.
var HELP_MESSAGE = "You can ask about a stocks performance over past 5 years or compare multiple stocks. So what would you like to know ?";


var states = {
    START: "_START",
    SYMBOL: "_SYMBOL"
};

const handlers = {
    "LaunchRequest": function () {
        this.handler.state = states.START;
        this.emitWithState("Start");
    },
    "MainIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("Symbol");
    },
    "HighestReturningStockIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("highestReturningStock");
    },
    "HighestGrowthYearIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("highestGrowthYear");
    },
    "CompareIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("compare");
    },
    "SeasonalIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("seasonal");
    },
    "BestMonthIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("bestMonth");
    },
    "BuyIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("shallIBuy");
    },
    "SellIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("shallISell");
    },
    "BestBuyIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("bestBuy");
    },
    "BestSellIntent": function () {
        this.handler.state = states.SYMBOL;
        this.emitWithState("bestSell");
    },
    "Unhandled": function () {
        console.log("Unhandled within core handlers");
        this.emit(":tell", "Sorry, I was not unable to recognize that, please try again.");
    }
};

var bestTrade = function (obj) {
    var reverseSymbCompMap = swap(symbolMap);
    var ret = {};
    for (var key in obj) {
        if (obj[key].toLowerCase().indexOf('none') == -1 && ret[obj[key]]) {
            ret[obj[key]]++;
        } else {
            ret[obj[key]] = 1;
        }
    }
    var maxCount = 0, maxSymbol;
    for (var k in ret) {
        if (ret[k] > maxCount) {
            maxSymbol = k;
            maxCount = ret[k];
        }
    }
    var r = reverseSymbCompMap[maxSymbol];
    return r;
};

var symbolHandlers = Alexa.CreateStateHandler(states.SYMBOL, {
    "bestBuy": function () {
        var years = this.event.request.intent.slots['yr'].value || 5;
        console.log("Best Buy : " + years);
        var obj = api.bestStockToBuy(years);
        var r = bestTrade(obj);
        var ymsg = years == 1 ? " year " : " years ";
        if (r && r.toLowerCase().indexOf('none') == -1) {
            this.emit(":tell", "Running " + TOTAL_TECH_INDICATORS + " technical indicators over past " + years + ymsg + ", I believe that, it's a good time to buy " + r + "'s stocks.");
        } else {
            this.emit(":tell", "Running " + TOTAL_TECH_INDICATORS + " technical indicators over past " + years + ymsg + ", I would suggest that it's not a good time to buy any stock.");
        }
    },
    "bestSell": function () {
        var years = this.event.request.intent.slots['yr'].value || 5;
        console.log("Best Sell : " + years);
        var obj = api.bestStockToSell(years);
        var r = bestTrade(obj);
        var ymsg = years == 1 ? " year " : " years ";
        if (r && r.toLowerCase().indexOf('none') == -1) {
            this.emit(":tell", "Running " + TOTAL_TECH_INDICATORS + " technical indicators over past " + years + ymsg + ", I believe that, it's a good time to sell " + r + "'s stocks.");
        } else {
            this.emit(":tell", "Running " + TOTAL_TECH_INDICATORS + " technical indicators over past " + years + ymsg + ", I would suggest that it's not a good time to sell any stock.");
        }
    },
    "shallIBuy": function () {
        var company = this.event.request.intent.slots['symb'].value;
        var symb = symbolMap[company.toLowerCase()];
        console.log("Shall I Buy " + company);
        var obj = api.averageInterpolator(symb, 5);
        var countBuys = 0;
        var total = Object.keys(obj).length;
        for (var k in obj) {
            if (obj[k] == CONSTANTS.BUY) {
                countBuys++;
            }
        }
        if (countBuys >= total / 2) {
            this.emit(":tell", "According to " + TOTAL_TECH_INDICATORS + " technical indicators, I would also recommend to buy " + company + "'s stock");
        } else {
            this.emit(":tell", "According to " + TOTAL_TECH_INDICATORS + " technical indicators, I would instead recommend to sell " + company + "'s stock");
        }
    },
    "shallISell": function () {
        var company = this.event.request.intent.slots['symb'].value;
        var symb = symbolMap[company.toLowerCase()];
        console.log("Shall I Sell " + company);
        var obj = api.averageInterpolator(symb, 5);
        var countSells = 0;
        var total = Object.keys(obj).length;
        for (var k in obj) {
            if (obj[k] == CONSTANTS.SELL) {
                countSells++;
            }
        }
        if (countSells >= total / 2) {
            this.emit(":tell", "According to " + TOTAL_TECH_INDICATORS + " technical indicators, I would also recommend to sell " + company + "'s stock");
        } else {
            this.emit(":tell", "According to " + TOTAL_TECH_INDICATORS + " technical indicators, I would instead recommend to buy " + company + "'s stock");
        }
    },
    "seasonal": function () {
        var company = this.event.request.intent.slots['symb'].value;
        var symb = symbolMap[company.toLowerCase()];
        console.log("Seasonal " + company);
        var best = api.bestMonthToBuyStock(symb);
        var worst = api.worstMonthToBuyStock(symb);
        this.emit(":tell", "Looking at past 5 years, the best month to buy " + company + "'s stocks is " + best + " . And the worst month is " + worst);
    },
    "bestMonth": function () {
        var company = this.event.request.intent.slots['symb'].value;
        var symb = symbolMap[company.toLowerCase()];
        console.log("bestMonth " + company);
        var best = api.bestMonthToBuyStock(symb);
        this.emit(":tell", "Looking at past 5 years, the best month to buy " + company + "'s stocks is " + best);
    },
    "compare": function () {
        var companyOne = this.event.request.intent.slots['symbOne'].value.toLowerCase();
        var symbOne = symbolMap[companyOne];
        var companyTwo = this.event.request.intent.slots['symbTwo'].value.toLowerCase();
        var symbTwo = symbolMap[companyTwo];
        var year = this.event.request.intent.slots['yr'].value;
        year = year || 5;       //default is 5 years
        var r = api.comparePerformance(symbOne, symbTwo, year);
        var ymsg = (year == 1) ? "year" : "years";
        console.log("comparePerformance : " + year);
        if (year <= 5) {
            if (r[0] > r[1]) {
                this.emit(":tell", "Over the past " + year + " " + ymsg + " , " + companyOne + " has grown by " + r[0] + " percent, where as " + companyTwo + " has grown by " + r[1]);
            } else {
                this.emit(":tell", "Over the past " + year + " " + ymsg + " , " + companyTwo + " has grown by " + r[1] + " percent, where as " + companyOne + " has grown by " + r[0]);
            }
        } else {
            if (r[0] > r[1]) {
                this.emit(":tell", "In " + year + " , " + companyOne + " has grown by " + r[0] + " percent, where as " + companyTwo + " grew by " + r[1]);
            } else {
                this.emit(":tell", "In " + year + " , " + companyTwo + " has grown by " + r[1] + " percent, where as " + companyOne + " grew by " + r[0]);
            }
        }
    },
    "Symbol": function () {
        var company = this.event.request.intent.slots['symb'].value;
        var symb = symbolMap[company.toLowerCase()];
        var year = this.event.request.intent.slots['year'].value;
        console.log("mainIntent - Symbol - fetchPerformance : " + company + "     " + year);

        if (year && !isNaN(year)) {
            let r = api.fetchPerformanceByYear(symb, year);
            let msg = "In year " + year + ", " + company + "'s stock";
            if (r > 0) {
                msg += " rose by " + r + " percent.";
            } else {
                msg += " fell by " + r + " percent."
            }
            this.emit(":tell", msg);
        } else {
            var result = api.performanceInFiveYears(symb);
            if (result > 0) {
                this.emit(":tell", "Over past five years, " + company + "'s stock rose by " + result + " percent.");
            } else {
                this.emit(":tell", "Over past five years, " + company + "'s stock fell by " + result + " percent");
            }
        }
    },
    "highestReturningStock": function () {
        var year = this.event.request.intent.slots['yr'].value;
        console.log("highestReturn  : " + year);
        var arr = api.highestReturningStock(year);
        var ymsg = (year == 1) ? 'year' : 'years';
        if (year <= 5) {
            this.emit(":tell", arr[0] + " is the highest performing stock over past " + year + " " + ymsg + ", with a performance of " + arr[1] + " percent");
        } else {
            this.emit(":tell", arr[0] + " is the highest performing stock in " + year + ", with a performance of " + arr[1] + " percent");
        }
    },
    "highestGrowthYear": function () {
        var company = this.event.request.intent.slots['symb'].value.toLowerCase();
        console.log("highestGrowthYearfor : " + company);
        var arr = api.highestGrowthYear(symbolMap[company]);
        this.emit(":tell", company + " had the largest growth in year " + arr[0] + ", which is " + arr[1] + " percent");
    },
    "AMAZON.StartOverIntent": function () {
        this.emit(":ask", HELP_MESSAGE, HELP_MESSAGE);
    },
    "AMAZON.StopIntent": function () {
        this.emit(":tell", EXIT_SKILL_MESSAGE);
    },
    "AMAZON.CancelIntent": function () {
        this.emit(":tell", EXIT_SKILL_MESSAGE);
    },
    "AMAZON.HelpIntent": function () {
        this.emit(":ask", HELP_MESSAGE, HELP_MESSAGE);
    },
    "Unhandled": function () {
        console.log("Unhandled within Symbol state");
        this.emit(":tell", "Sorry, I was not unable to recognize that, please try again.");
    }
});

var startHandlers = Alexa.CreateStateHandler(states.START, {
    "Start": function () {
        this.emit(":tell", WELCOME_MESSAGE, HELP_MESSAGE);
    },
    "AMAZON.StopIntent": function () {
        this.emit(":tell", EXIT_SKILL_MESSAGE);
    },
    "AMAZON.CancelIntent": function () {
        this.emit(":tell", EXIT_SKILL_MESSAGE);
    },
    "AMAZON.HelpIntent": function () {
        this.emit(":ask", HELP_MESSAGE, HELP_MESSAGE);
    },
    "Unhandled": function () {
        console.log("Unhandled within start state");
        this.emit(":tell", "Sorry, I was not unable to recognize that, please try again.");
    }
});


function swap(json) {
    var ret = {};
    for (var key in json) {
        ret[json[key]] = key;
    }
    return ret;
}

exports.handler = (event, context) => {
    const alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    alexa.registerHandlers(handlers, startHandlers, symbolHandlers);
    alexa.execute();
};

