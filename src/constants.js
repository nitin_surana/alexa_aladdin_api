var obj = {
    SELL: "sell",
    BUY: "buy",
    SMA: "SimpleMovingAverage",
    EMA: "ExponentialMovingAverage",
    WMA: "WeightedMovingAverage"
};
module.exports = obj;
